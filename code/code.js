// Додати нові продукти (Відео для відео хостингу та страви для ресторану)
// Відео має бути 3 зовнішнім посиланням та 3 відео завантажені до проекту 

// Тут реалізація вашого коду. 

import { generationId, dateNow } from "./functions.js";

export function saveDataOther() {
   try {
       const [isCategory] = document.querySelector("select").selectedOptions;
       const [...inputs] = document.querySelectorAll("form input");
       if (isCategory.value === "Відео хостинг") {
           const obj = {
               productName: "string",               
               poster: "string",
               url: "string",          
               description: "string",
               keywords: "string array",
           };

           inputs.forEach(e => {
               obj[e.dataset.type] = e.value;
               e.value = ''
           })

           const video = JSON.parse(localStorage.video);
           video.push(new VideoHostElementCRM(
               obj.productName,               
               obj.poster,
               obj.url,
               obj.description,
               undefined,
               obj.keywords,
               dateNow,
               generationId));

           localStorage.video = JSON.stringify(video);

       } else if (isCategory.value === "Рестаран") {
        const obj = {
            productName: "string",
            productWeiht: "string",
            ingredients: "string",
            description: "string",
            keywords: "string array",
            price: "number",
            productImageUrl: "string",
            weiht: "string",
        };

        inputs.forEach(e => {
            obj[e.dataset.type] = e.value;
            e.value = ''
        })

        const rest = JSON.parse(localStorage.restorationBD);
        rest.push(new RestoranElementCRM(
            obj.productName,
            obj.productWeiht,
            obj.ingredients,
            obj.description,
            obj.keywords,
            obj.price,
            obj.productImageUrl,
            obj.weiht,
            undefined,
            dateNow,
            generationId));

        localStorage.restorationBD = JSON.stringify(rest);

    }
   } catch (e) {
       console.error(e)
   }
}

class VideoHostElementCRM {
   constructor (productName = "", poster = "/img/error.png", url = "/video/error.png", description = "", like = 0, keywords = [], dateNow = () => {}, id = () => {}) {
       this.id = id();
       this.date = dateNow();
       this.productName = productName;
       this.like = like;       
       this.poster = poster;
       this.url = url;
       this.description = description;
       this.keywords = keywords.split(",");
       this.status = false
   }
}

class RestoranElementCRM {
    constructor (productName = "", productWeiht = "", ingredients = "", description = "", keywords = [], price = 0, productImageUrl = "/img/error.png", weiht = "", like = 0, dateNow = () => {}, id = () => {}) {
        this.id = id();
        this.date = dateNow();
        this.productName = productName;
        this.productWeiht = productWeiht;
        this.ingredients = ingredients;
        this.description = description;
        this.keywords = keywords.split(",");
        this.price = price;
        this.productImageUrl = productImageUrl;
        this.weiht = weiht;
        this.like = like;
        this.stopList = false;
        this.ageRestrictions = false
    }
 }