import { getLogin, getPassword, modalClose, modalSave } from "./var.js";
import { changeInputEvent, userLoginEvent, showModalEvent, hideModalEvent, saveData, exportDataEvent, userLogoutEvent } from "./events.js";
import { req, categorySelect } from "./functions.js";
import { saveDataOther } from "./code.js";

if (!sessionStorage.isLogin && !document.location.pathname.includes("/authorization")) {
    document.location = "/authorization";
}
//authorization
try {
    document.querySelector(".window form")
        .addEventListener("change", changeInputEvent);

    document.getElementById("disabled")
        .addEventListener("click", userLoginEvent)
} catch (error) {
    // console.error(error)
}

//main page
try {
    document.querySelector(".add")
        .addEventListener("click", showModalEvent);

    const modalWindow = document.querySelector(".container-modal .modal");    

    modalWindow.insertAdjacentHTML("beforeend", `
    <h2>Додайте новий продукт до БД</h2>
    <div class="catigoty"></div>
    <div class="modal__body"></div>
    <div class="modal__control"></div>
    `)

    document.querySelector(".modal__control").append(modalSave, modalClose);
    categorySelect();

    modalClose.addEventListener("click", hideModalEvent);

    modalSave.addEventListener("click", saveData);

    modalSave.addEventListener("click", saveDataOther)

    /*
    Ваші події 
    */

} catch (e) {
    // console.error(e)
}

try {
    const EXPORT = document.querySelector("#export");
    const REQ = document.getElementById("req");

    EXPORT.addEventListener("click", exportDataEvent);
    REQ.addEventListener("click", () => {
        req("fetch", "https://jsonplaceholder.typicode.com/comments");
    });
} catch (error) {
    //console.error(error)
}

//logout
try {
    document.querySelector("#logout")
        .addEventListener("click", userLogoutEvent);
} catch (e) {
    //console.log(e);
}


console.log(getLogin, getPassword);

if(!localStorage.store){
    localStorage.store = JSON.stringify([])
}
if(!localStorage.video){
    localStorage.video = JSON.stringify([])
}
if(!localStorage.restorationBD){
    localStorage.restorationBD = JSON.stringify([])
}

