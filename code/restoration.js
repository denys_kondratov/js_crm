import { hideModalEvent, showModalEvent } from "./events.js";
import { createHTMLElement, createEditProductInput } from "./functions.js";
import { modalClose, modalSave } from "./var.js";

//console.log(JSON.parse(localStorage.restorationBD));

//Вивід на сторінку позицій меню
function showRestoranMenu(arr = []) {
    //Знайшли tbody для виводу інформації по позиціям 
    const tbody = document.querySelector("tbody");
    tbody.innerHTML = "";

    arr.forEach(function ({productName, like, price, stopList, date, id}, i) {
        //Назва	Залишок	Ціна	Редагувати	Статус	Дата додавання	Видалити
        const tr = createHTMLElement("tr");
        const element = [
            createHTMLElement("td", undefined, i + 1),
            createHTMLElement("td", undefined, productName),
            createHTMLElement("td", undefined, like),
            createHTMLElement("td", undefined, price),
            createHTMLElement("td", undefined, `<span data-key="${id}" class="icon">&#9998;</span>`, undefined, editProductRestorationEvent),
            createHTMLElement("td", undefined, stopList? "<span class='icon green'>&#10004;</span>" : "<span class='icon red'>&#10008;</span>"),
            createHTMLElement("td", undefined, date),
            createHTMLElement("td", undefined, `<span data-key="${id}" class="icon">&#10006;</span>`, undefined, deleteProductRestorationEvent)
        ]
        tbody.append(tr);
        tr.append(...element);
    })
}

// Читаємо з localStorage
if (localStorage.restorationBD) {
    showRestoranMenu(JSON.parse(localStorage.restorationBD));
}

// Змінюємо продукут з БД
function editProductRestorationEvent (e) {
    if (e.target.tagName !== "SPAN") return;
    showModalEvent();

    const span = e.target;
    const restorationBD = JSON.parse(localStorage.restorationBD);

    const modalWindow = document.querySelector(".modal");
    const modalBody = createHTMLElement("div", "modal-body");
    modalWindow.append(modalBody);

    // Робота з кнопками 
    const btns = createHTMLElement("div", "btns-save");

    modalSave.addEventListener("click", () => {
        newSaveProductInfo(modalBody, rez);        
        hideModalEvent();
        modalBody.remove();
        showRestoranMenu(JSON.parse(localStorage.restorationBD));
    });

    modalClose.addEventListener("click", () => {
        hideModalEvent();
        modalBody.remove();
    });

    btns.append(modalSave, modalClose);
    modalWindow.append(btns);

    //Визначення обєвкта для редагування
    const rez = restorationBD.find((a)=>{
        return span.dataset.key === a.id
    });
    const data = Object.entries(rez);

    console.log(data);

    const dataModified = data.map(([props, value]) => {
        let container = [];
        
        switch(props) {
            case "productName" : props = "Назва Страви";
            container[0] = props;
            container[1] = value;
            break
            case "productWeiht" : props = "Грамовка";
            container[0] = props;
            container[1] = value;
            break
            case "ingredients" : props = "Склад";
            container[0] = props;
            container[1] = value;
            break
            case "description" : props = "Опис продукту";
            container[0] = props;
            container[1] = value;
            break
            case "keywords" : props = "Ключеві слова для пошуку. Розділяти комою";
            container[0] = props;
            container[1] = value;
            break
            case "price" : props = "Вартість продукту";
            container[0] = props;
            container[1] = value;
            break
            case "productImageUrl" : props = "Забраження продукту";
            container[0] = props;
            container[1] = value;
            break
            case "weiht" : props = "Вага продукту";
            container[0] = props;
            container[1] = value;
            break
            case "id" : props = "id";
            container[0] = props;
            container[1] = value;
            break
            case "date" : props = "date";
            container[0] = props;
            container[1] = value;
            break
            case "like" : props = "like";
            container[0] = props;
            container[1] = value;
            break
            case "stopList" : props = "stopList";
            container[0] = props;
            container[1] = value;
            break
            case "ageRestrictions" : props = "ageRestrictions";
            container[0] = props;
            container[1] = value;
            break
        } 
        return container; 
    });

    // Редагування позиції
    const inputsElemets = dataModified.map(([props, value]) => {
       return createEditProductInput(props, value)
    })
    modalBody.append(...inputsElemets);
}

function newSaveProductInfo (newObj, oldObj) {
    const inputs = newObj.querySelectorAll("input");

    const obj = {
        id: oldObj.id,
        date: oldObj.date,
        stopList: false,
        ageRestrictions: false
    }

    inputs.forEach(input => {
        switch(input.key) {
            case "Вартість продукту" : obj.price = input.value;
            return
            case "Склад" : obj.ingredients = input.value;
            return
            case "Забраження продукту" : obj.productImageUrl = input.value;
            return
            case "Назва Страви" : obj.productName = input.value;
            return
            case "like" : obj.like = input.value;
            return
            case "Грамовка" : obj.productWeiht = input.value;
            return
            case "Ключеві слова для пошуку. Розділяти комою" : obj.keywords = input.value.split(",");
            return
            case "Опис продукту" : obj.description = input.value;
            return
            case "Вага продукту" : obj.weiht = input.value;
            return
        }
    })
    if (obj.like > 0) {
       obj.stopList = true;
    } else {
        obj.stopList = false;
        obj.like = 0;
    }
    const restorationBD = JSON.parse(localStorage.restorationBD);
    restorationBD.splice(restorationBD.findIndex(el=>el.id === oldObj.id), 1, obj);
    localStorage.restorationBD = JSON.stringify(restorationBD);    
}

// Видалення страви з БД
function deleteProductRestorationEvent (e) {
    if(e.target.tagName !== "SPAN") return;

    const span = e.target;
    const restorationBD = JSON.parse(localStorage.restorationBD);

    const rez = restorationBD.find((a)=>{
        return span.dataset.key === a.id
    });

    restorationBD.splice(restorationBD.findIndex(el=>el.id === rez.id), 1);
    localStorage.restorationBD = JSON.stringify(restorationBD);
    showRestoranMenu(JSON.parse(localStorage.restorationBD));
}

/*
[{"id":"4a$o_bcj2mq","productName":"Борщ","productWeiht":"400","ingredients":
"українська рідка страва, що вариться з посічених буряків, капусти з 
додатком картоплі та різних приправ[","price":"40","productImageUrl":
"https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Borscht_served.jpg/440px-Borscht_served.jpg",
"keywords":["Гарячі страви"," Чеврний борщ"," з буряком"],
"stopList":true,"quantity":0,"date":"\n    2023-1-25 20:6:11\n "}]
*/

