import { hideModalEvent, showModalEvent } from "./events.js";
import { createHTMLElement, createEditProductInput } from "./functions.js";
import { modalClose, modalSave } from "./var.js";

//console.log(JSON.parse(localStorage.restorationBD));

//Вивід на сторінку позицій меню
function showStoreProduct(arr = []) {
    //Знайшли tbody для виводу інформації по позиціям 
    const tbody = document.querySelector("tbody");
    tbody.innerHTML = "";

    arr.forEach(function ({productName, productQuantity, porductPrice, status, date, id}, i) {
        //#	Назва	Залишок	Ціна	Редагувати	Статус	Дата додавання	Видалити
        const tr = createHTMLElement("tr");
        const element = [
            createHTMLElement("td", undefined, i + 1),
            createHTMLElement("td", undefined, productName),
            createHTMLElement("td", undefined, productQuantity),
            createHTMLElement("td", undefined, porductPrice),
            createHTMLElement("td", undefined, `<span data-key="${id}" class="icon">&#9998;</span>`, undefined, editProductStoreEvent),
            createHTMLElement("td", undefined, status? "<span class='icon green'>&#10004;</span>" : "<span class='icon red'>&#10008;</span>"),
            createHTMLElement("td", undefined, date),
            createHTMLElement("td", undefined, `<span data-key="${id}" class="icon">&#10006;</span>`, undefined, deleteProductStoreEvent),
        ]
        tbody.append(tr);
        tr.append(...element)
    })
}

// Читаємо з localStorage
if (localStorage.store) {
    showStoreProduct(JSON.parse(localStorage.store));
}

// Змінюємо продукут з БД
function editProductStoreEvent (e) {
    if(e.target.tagName !== "SPAN") return;
    showModalEvent();

    const span = e.target;
    const store = JSON.parse(localStorage.store);

    const modalWindow = document.querySelector(".modal");
    const modalBody = createHTMLElement("div", "modal-body");
    modalWindow.append(modalBody);

     // Робота з кнопками 
    const btns = createHTMLElement("div", "btns-save");

    modalSave.addEventListener("click", () => {
        newSaveProductInfo(modalBody, rez);
        hideModalEvent();
        modalBody.remove();
        showStoreProduct(JSON.parse(localStorage.store));
    });

    modalClose.addEventListener("click", () => {
        hideModalEvent();
        modalBody.remove();
    });

    btns.append(modalSave, modalClose);
    modalWindow.append(btns)

    //Визначення обєвкта для редагування
    const rez = store.find((a)=>{
        return span.dataset.key === a.id
    });
    const data = Object.entries(rez);

    const dataModified = data.map(([props, value]) => {
        let container = [];
        
        switch(props) {
            case "productName" : props = "Назва товару";
            container[0] = props;
            container[1] = value;
            break
            case "porductPrice" : props = "Вартість товару";
            container[0] = props;
            container[1] = value;
            break
            case "productImage" : props = "Картинка товару";
            container[0] = props;
            container[1] = value;
            break
            case "productDescription" : props = "Опис товару";
            container[0] = props;
            container[1] = value;
            break
            case "keywords" : props = "Ключеві слова для пошуку. Розділяти комою";
            container[0] = props;
            container[1] = value;
            break
            case "id" : props = "id";
            container[0] = props;
            container[1] = value;
            break
            case "date" : props = "date";
            container[0] = props;
            container[1] = value;
            break
            case "productQuantity" : props = "Кількість товару";
            container[0] = props;
            container[1] = value;
            break
            case "status" : props = "status";
            container[0] = props;
            container[1] = value;
            break
        } 
        return container; 
    });

    // Редагування позиції
    const inputsElemets = dataModified.map(([props, value]) => {
       return createEditProductInput(props, value)
    })
    modalBody.append(...inputsElemets)
}

function newSaveProductInfo (newObj, oldObj) {
    const inputs = newObj.querySelectorAll("input");

    const obj = {
        id : oldObj.id,
        date: oldObj.date,
        status: false
    }

    inputs.forEach(input => {
        switch(input.key) {
            case "Вартість товару" : obj.porductPrice = input.value;
            return
            case "Опис товару" : obj.productDescription = input.value;
            return
            case "Картинка товару" : obj.productImage = input.value;
            return
            case "Назва товару" : obj.productName = input.value;
            return
            case "Кількість товару" : obj.productQuantity = input.value;
            return
            case "Ключеві слова для пошуку. Розділяти комою" : obj.keywords = input.value.split(",");
            return
        }
    })
    if(obj.productQuantity > 0){
       obj.status = true;
    }else{
        obj.status = false;
        obj.productQuantity = 0;
    }
    const store = JSON.parse(localStorage.store);
    store.splice(store.findIndex(el=>el.id === oldObj.id), 1, obj);
    localStorage.store = JSON.stringify(store);
}

// Видалення товару з БД
function deleteProductStoreEvent (e) {
    if(e.target.tagName !== "SPAN") return;

    const span = e.target;
    const store = JSON.parse(localStorage.store);

    const rez = store.find((a)=>{
        return span.dataset.key === a.id
    });

    store.splice(store.findIndex(el=>el.id === rez.id), 1);
    localStorage.store = JSON.stringify(store);
    showStoreProduct(JSON.parse(localStorage.store));
}

