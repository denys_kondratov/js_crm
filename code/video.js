import { hideModalEvent, showModalEvent } from "./events.js";
import { createHTMLElement, createEditProductInput } from "./functions.js";
import { modalClose, modalSave } from "./var.js";

//console.log(JSON.parse(localStorage.video));

// Вивід на сторінку відеофайлів
function showVideoFiles(arr = []) {
    //Знайшли tbody для виводу інформації по позиціям 
    const tbody = document.querySelector("tbody");
    tbody.innerHTML = "";

    arr.forEach(function ({productName, date, url, id}, i) {
        //# 	Назва 	Дата публікації 	Посилання 	Редагувати 	Видалити
        const tr = createHTMLElement("tr");
        const element = [
            createHTMLElement("td", undefined, i + 1),
            createHTMLElement("td", undefined, productName),
            createHTMLElement("td", undefined, date),
            createHTMLElement("td", undefined, `<a class="video_url" target="_blank" href="${url}">Перейти</a>`),
            createHTMLElement("td", undefined, `<span data-key="${id}" class="icon">&#9998;</span>`, undefined, editVideoFileEvent),
            createHTMLElement("td", undefined, `<span data-key="${id}" class="icon">&#10006;</span>`, undefined, deleteVideoFileEvent)
        ]
        tbody.append(tr);
        tr.append(...element);
    })
}

//Читаємо з localStorage
if (localStorage.video) {
    showVideoFiles(JSON.parse(localStorage.video));
}

// Змінюємо продукут з БД
function editVideoFileEvent (e) {
    if (e.target.tagName !== "SPAN") return;
    showModalEvent();

    const span = e.target;
    const video = JSON.parse(localStorage.video);

    const modalWindow = document.querySelector(".modal");
    const modalBody = createHTMLElement("div", "modal-body");
    modalWindow.append(modalBody);

    // Робота з кнопками 
    const btns = createHTMLElement("div", "btns-save");

    modalSave.addEventListener("click", () => {
        newSaveVideoInfo(modalBody, rez);
        hideModalEvent();
        modalBody.remove();      
        showVideoFiles(JSON.parse(localStorage.video));
    });

    modalClose.addEventListener("click", () => {
        hideModalEvent();
        modalBody.remove();
    });

    btns.append(modalSave, modalClose);
    modalWindow.append(btns);

    //Визначення обєвкта для редагування
    const rez = video.find((a)=>{
        return span.dataset.key === a.id
    });
    const data = Object.entries(rez);

    const dataModified = data.map(([props, value]) => {
        let container = [];
        
        switch(props) {
            case "productName" : props = "Назва відео";
            container[0] = props;
            container[1] = value;
            break
            case "poster" : props = "Постер";
            container[0] = props;
            container[1] = value;
            break
            case "url" : props = "Посилання на відео";
            container[0] = props;
            container[1] = value;
            break
            case "description" : props = "Опис продукту";
            container[0] = props;
            container[1] = value;
            break
            case "keywords" : props = "Ключеві слова для пошуку. Розділяти комою";
            container[0] = props;
            container[1] = value;
            break
            case "id" : props = "id";
            container[0] = props;
            container[1] = value;
            break
            case "date" : props = "date";
            container[0] = props;
            container[1] = value;
            break
            case "like" : props = "like";
            container[0] = props;
            container[1] = value;
            break
            case "status" : props = "status";
            container[0] = props;
            container[1] = value;
            break
        } 
        return container; 
    });
    
    // Редагування позиції
    const inputsElemets = dataModified.map(([props, value]) => {
        return createEditProductInput(props, value)
    });
    modalBody.append(...inputsElemets);
}

function newSaveVideoInfo (newObj, oldObj) {
    const inputs = newObj.querySelectorAll("input");

    const obj = {
        id: oldObj.id,
        date: oldObj.date,
        status: false
    }

    inputs.forEach(input => {
        switch(input.key) {
            case "Назва відео" : obj.productName = input.value;
            return
            case "Опис продукту" : obj.description = input.value;
            return
            case "Ключеві слова для пошуку. Розділяти комою" : obj.keywords = input.value.split(",");
            return
            case "Постер" : obj.poster = input.value;
            return
            case "like" : obj.like = input.value;
            return
            case "Посилання на відео" : obj.url = input.value;
            return
        }
    })
    if (obj.like > 0) {
        obj.status = true;
    } else {
        obj.status = false;
        obj.like = 0;
    }
    const video = JSON.parse(localStorage.video);
    video.splice(video.findIndex(el=>el.id === oldObj.id), 1, obj);
    localStorage.video = JSON.stringify(video);
}

// Видалення відеофайлу з БД
function deleteVideoFileEvent (e) {
    if(e.target.tagName !== "SPAN") return;

    const span = e.target;
    const video = JSON.parse(localStorage.video);

    const rez = video.find((a)=>{
        return span.dataset.key === a.id
    });

    video.splice(video.findIndex(el=>el.id === rez.id), 1);
    localStorage.video = JSON.stringify(video);
    showVideoFiles(JSON.parse(localStorage.video));
}

/*
date: "2023/2/7 15:41:1"​​
description: "Дикая природа Амазонки HD"​​
id: "w&ep580rr6ldvyh1j"​​
keywords: Array(4) [ "HD", " Природа", " Дикая", … ]​​
like: 0​​
poster: "https://i1.ytimg.com/vi/X8Z8okhkjv8/hqdefault.jpg"​​
productName: "Дикая природа"​​
status: false​​
url: "https://www.youtube.com/watch?v=X8Z8okhkjv8"
*/