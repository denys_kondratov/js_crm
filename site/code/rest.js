const rest = JSON.parse(localStorage.restorationBD);

// console.log(rest);

if(!Array.isArray(rest)){
    throw Error("Не масив")
}

const restEl = rest.map(({ id, productImageUrl, productName, description, ingredients, weiht, price }) => {

    return `
    <div id="${id}" class="food">
        <a href="#" class="food__link_img"><img src="${productImageUrl}" alt="img" class="link_img"></a>
        <div class="food__content">
            <h3 class="content_title">${productName}</h3>
            <p class="content_discript">${description}</p>
            <p class="content_ingredients">Склад: ${ingredients}</p>
            <div class="content_info">
                <div class="content_weiht">${weiht} / </div>
                <div class="content_price">${price} грн</div>
            </div>
        </div>
    </div>
    `
})

document.querySelector(".rest-box")
.insertAdjacentHTML("beforeend", restEl.join(""));