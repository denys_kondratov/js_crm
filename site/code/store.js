const store = JSON.parse(localStorage.store);

// console.log(store);

if(!Array.isArray(store)){
    throw Error("Не масив")
}

const storeEl = store.map(({ id, productImage, productName, porductPrice }) => {

    return `
    <div id="${id}" class="product">
        <a class="product__link_img" href="#"><img class="link_img" src="${productImage}" alt="img"></a>
        <a href="#" class="product__title"><span class="title">${productName}</span></a>
        <div class="product__price">${porductPrice} грн</div>
    </div>
    `
})

document.querySelector(".store-box")
.insertAdjacentHTML("beforeend", storeEl.join(""));