const video = JSON.parse(localStorage.video);

// console.log(video);

if(!Array.isArray(video)){
    throw Error("Не масив")
}

const videoEl = video.map(({ productName, id, url, description, keywords, poster }) => {

    return `
    <div class="video">
    <h3 class="video-name">${productName}</h3>
    ${poster.startsWith("../img") ? `<video id="${id}" controls poster="../${poster}">`: `<video id="${id}" controls poster="${poster}">`}
        ${url.startsWith("../videofile") ? `<source src="../${url}">`: `<source src="${url}">`}
    </video>    
    <p class="video-description">
    ${description}
    </p>
    <div>
    ${keywords.map((el) => {
        return `<span class="badge bg-secondary">${el}</span>`
    }).join("")}
     </div>  
    </div>
    `
})

document.querySelector(".video-box")
.insertAdjacentHTML("beforeend", videoEl.join(""));